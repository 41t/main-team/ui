import type { Meta, StoryObj } from '@storybook/svelte';
import Button from '../lib/Button.svelte';
import { Area } from '../lib/enums/Area';
import { ButtonType } from '../lib/enums/ButtonType';

const meta = {
  title: '41T/Button',
  component: Button,
  tags: ['autodocs'],
  argTypes: {
    area: {
      control: { type: 'select' },
      options: Area
    },
    type: {
      control: { type: 'select' },
      options: ButtonType
    }
  }
} satisfies Meta<Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const ORG: Story = {
  args: {
    area: Area.ORG,
    label: 'Button'
  }
};

export const DEV: Story = {
  args: {
    area: Area.DEV,
    label: 'Button'
  }
};

export const LABS: Story = {
  args: {
    area: Area.LABS,
    label: 'Button'
  }
};

export const DESIGN: Story = {
  args: {
    area: Area.DESIGN,
    label: 'Button'
  }
};

export const SPACE: Story = {
  args: {
    area: Area.SPACE,
    label: 'Button'
  }
};
