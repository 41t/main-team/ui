import type { Preview } from "@storybook/svelte";
import '../src/app.css';
import { withThemeByClassName, withThemeByDataAttribute } from '@storybook/addon-styling';

export const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/
      }
    }
  }
};

export const decorators = [
  withThemeByClassName({
    themes: {
      light: 'light',
      dark: 'dark'
    },
    defaultTheme: 'light',
    parentSelector: 'body'
  })
];