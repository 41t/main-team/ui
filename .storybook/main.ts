import type { StorybookConfig } from "@storybook/svelte-vite";

const config: StorybookConfig = {
  stories: ["../src/**/*.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx|svelte)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    {
      name: '@storybook/addon-styling',
      options: { postCss: true }
    }
  ],
  framework: {
    name: "@storybook/svelte-vite",
    options: {}
  },
  docs: {
    autodocs: "tag"
  }
};
export default config;
